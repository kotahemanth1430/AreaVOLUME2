#include<iostream>
using namespace std;
class Box{
    public:
    float len,wid,hei;
    void names(float len1,float wid1,float hei1)
    {
        len=len1;
        wid=wid1;
        hei=hei1;
    }
    void boxArea(float length,float width,float height)
    {
        cout<<"Area : "<<float(2*((length*width)+(width*height)+(length*height)))<<endl;
    }
    void boxVolume(float length,float width,float height);
    friend void boxdisplayDimensions(Box);
    inline void displaymessage();
};

void Box::boxVolume(float length,float width,float height)
{
    cout<<"Volume: "<<float(length*width*height)<<endl;
}
void displayDimensions(Box b)
{
    cout<<"length: "<<b.len<<endl;
    cout<<"width: "<<b.wid<<endl;
    cout<<"height: "<<b.hei<<endl;

}
inline void Box::displaymessage()
{
    cout<<"welcome to program!"<<endl;
}
int main()
{
    float l,w,h;
    Box b;
    cout<<"enter the length: "<<endl;
    cin>>l;
    cout<<"enter the width: "<<endl;
    cin>>w;
    cout<<"enter the height: "<<endl;
    cin>>h;
    Box r;
    r.boxArea(l,w,h);
    r.boxVolume(l,w,h);
    r.names(l,w,h);
    displayDimensions(r);
    r.displaymessage();
}